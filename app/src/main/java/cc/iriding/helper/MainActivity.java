package cc.iriding.helper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import cc.iriding.smarthelper.CustomUtil;

public class MainActivity extends AppCompatActivity {
    TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("1"+CustomUtil.getHello());
    }
}
